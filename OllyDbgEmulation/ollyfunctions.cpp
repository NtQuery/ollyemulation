#define _CRT_SECURE_NO_WARNINGS
#include <windows.h>
#include <stdio.h>
#include <string.h>
#include "Plugin.h"

HINSTANCE hInstance = 0;
HWND hWndMain = 0;
HWND hWndClient = 0;
HANDLE hProcess = 0;
CHAR OllyFileName[260];

char aPluginS[] = "Plugin %s";

BOOL WINAPI DllMain(HINSTANCE hinstDLL,	DWORD fdwReason, LPVOID lpReserved )
{
	// Perform actions based on the reason for calling.
	switch( fdwReason ) 
	{ 
	case DLL_PROCESS_ATTACH:
		// Initialize once for each new process.
		// Return FALSE to fail DLL load.
		hInstance = hinstDLL;
		break;

	case DLL_THREAD_ATTACH:
		// Do thread-specific initialization.
		break;

	case DLL_THREAD_DETACH:
		// Do thread-specific cleanup.
		break;

	case DLL_PROCESS_DETACH:
		// Perform any necessary cleanup.
		break;
	}
	return TRUE;  // Successful DLL_PROCESS_ATTACH.
}

extc void    cdecl _Addtolist(long addr,int highlight,char *format,...)
{

}
extc void    cdecl _Updatelist(void)
{

}
extc HWND    cdecl _Createlistwindow(void)
{

	return 0;
}
extc void    cdecl _Error(char *format,...)
{

}
extc void    cdecl _Message(ulong addr,char *format,...)
{

}
extc void    cdecl _Infoline(char *format,...)
{

}
extc void    cdecl _Progress(int promille,char *format,...)
{

}
extc void    cdecl _Flash(char *format,...)
{

}

extc int     cdecl _Getlong(char *title,ulong *data,int datasize,
	char letter,int mode){

		return 0;
}
extc int     cdecl _Getlongxy(char *title,ulong *data,int datasize,
	char letter,int mode,int x,int y){

		return 0;
}
extc int     cdecl _Getregxy(char *title,ulong *data,char letter,int x,int y){

	return 0;
}
extc int     cdecl _Getline(char *title,ulong *data){

	return 0;
}
extc int     cdecl _Getlinexy(char *title,ulong *data,int x,int y){

	return 0;
}
extc int     cdecl _Getfloat10(char *title,long double *fdata,
	uchar *tag,char letter,int mode){

		return 0;
}
extc int     cdecl _Getfloat10xy(char *title,long double *fdata,
	char *tag,char letter,int mode,int x,int y){

		return 0;
}
extc int     cdecl _Getfloat(char *title,void *fdata,int size,
	char letter,int mode){

		return 0;
}
extc int     cdecl _Getfloatxy(char *title,void *fdata,int size,
	char letter,int mode,int x,int y){

		return 0;
}
extc void    cdecl _Getasmfindmodel(t_asmmodel model[NMODELS],
	char letter,int searchall){

}
extc void    cdecl _Getasmfindmodelxy(t_asmmodel model[NMODELS],
	char letter,int searchall,int x,int y){

}
extc int     cdecl _Gettext(char *title,char *text,
	char letter,int type,int fontindex){

		return 0;
}
extc int     cdecl _Gettextxy(char *title,char *text,char letter,
	int type,int fontindex,int x,int y){

		return 0;
}
extc int     cdecl _Gethexstring(char *title,t_hexstr *hs,
	int mode,int fontindex,char letter){

		return 0;
}
extc int     cdecl _Gethexstringxy(char *title,t_hexstr *hs,int mode,
	int fontindex,char letter,int x,int y){

		return 0;
}
extc int     cdecl _Getmmx(char *title,uchar *data,int mode){return 0;}
extc int     cdecl _Getmmxxy(char *title,char *data,int mode,int x,int y) { return 0; }
extc int     cdecl _Get3dnow(char *title,uchar *data,int mode){

	return 0;
}
extc int     cdecl _Get3dnowxy(char *title,char *data,int mode,int x,int y){

	return 0;
}
extc int     cdecl _Browsefilename(char *title,char *name,char *defext,
	int getarguments){

		return 0;
}
extc int     cdecl _OpenEXEfile(char *path,int dropped){

	return 0;
}
extc int     cdecl _Attachtoactiveprocess(int newprocessid){

	return 0;
}
extc void    cdecl _Animate(int animation){

}

extc int     cdecl _Createsorteddata(t_sorted *sd,char *name,int itemsize,
	int nmax,SORTFUNC *sortfunc,DESTFUNC *destfunc){

		return 0;
}
extc void    cdecl _Destroysorteddata(t_sorted *sd){

}
extc void    cdecl *_Addsorteddata(t_sorted *sd,void *item){
	return 0;
}
extc void    cdecl _Deletesorteddata(t_sorted *sd,ulong addr){

}
extc void    cdecl _Deletesorteddatarange(t_sorted *sd,ulong addr0,ulong addr1){

}
extc int     cdecl _Deletenonconfirmedsorteddata(t_sorted *sd){

	return 0;
}
extc void*   cdecl _Findsorteddata(t_sorted *sd,ulong addr){

	return 0;
}
extc void*   cdecl _Findsorteddatarange(t_sorted *sd,ulong addr0,ulong addr1){

	return 0;
}
extc int     cdecl _Findsorteddataindex(t_sorted *sd,ulong addr0,ulong addr1){

	return 0;
}
extc int     cdecl _Sortsorteddata(t_sorted *sd,int sort){

	return 0;
}
extc void*   cdecl _Getsortedbyselection(t_sorted *sd,int index){

	return 0;
}
extc void    cdecl _Defaultbar(t_bar *pb){}
extc int     cdecl _Tablefunction(t_table *pt, HWND hw,UINT msg,WPARAM wp,LPARAM lp){ return 0;}
extc void    cdecl _Painttable(HWND hw,t_table *pt,DRAWFUNC getline){}
extc int     cdecl _Gettableselectionxy(t_table *pt,int column,int *px,int *py){ return 0;}
extc void    cdecl _Selectandscroll(t_table *pt,int index,int mode){}
extc int     cdecl _Insertname(ulong addr,int type,char *name){ return 0;}
extc int     cdecl _Quickinsertname(ulong addr,int type,char *name){ return 0;}
extc void    cdecl _Mergequicknames(void){}
extc void    cdecl _Discardquicknames(void){}
extc int     cdecl _Findname(ulong addr,int type,char *name){ return 0;}
extc int     cdecl _Decodename(ulong addr,int type,char *name){ return 0;}
extc ulong   cdecl _Findnextname(char *name){ return 0;}
extc int     cdecl _Findlabel(ulong addr,char *name){ return 0;}
extc void    cdecl _Deletenamerange(ulong addr0,ulong addr1,int type){}
extc int     cdecl _Findlabelbyname(char *name,ulong *addr,	ulong addr0,ulong addr1){ return 0;}
extc ulong   cdecl _Findimportbyname(char *name,ulong addr0,ulong addr1){ return 0;}
extc int     cdecl _Demanglename(char *name,int type,char *undecorated){ return 0;}
extc int     cdecl _Findsymbolicname(ulong addr,char *fname){ return 0; }
extc int     cdecl _Expression(t_result *result,char *expression,int a,int b,uchar *data,ulong database,ulong datasize,ulong threadid){ return 0; }
extc HWND    cdecl _Createthreadwindow(void){ return 0; }
extc t_thread* cdecl _Findthread(ulong threadid){ return 0; }
extc int     cdecl _Decodethreadname(char *s,ulong threadid,int mode){ return 0; }
extc ulong   cdecl _Getcputhreadid(void){ return 0; }
extc ulong   cdecl _Runsinglethread(ulong threadid){ return 0; }
extc void    cdecl _Restoreallthreads(void){}
extc int     cdecl _Listmemory(void){ return 0; }
extc t_memory* cdecl _Findmemory(ulong addr){ return 0; }
extc int     cdecl _Guardmemory(ulong base,ulong size,int guard){ return 0; }
extc void    cdecl _Havecopyofmemory(uchar *copy,ulong base,ulong size){}
extc ulong   cdecl _Readmemory(void *buf,ulong addr,ulong size,int mode){ return 0; }
extc ulong   cdecl _Writememory(void *buf,ulong addr,ulong size,int mode){ return 0; }
extc ulong   cdecl _Readcommand(ulong ip,char *cmd){ return 0; }
extc t_module* cdecl _Findmodule(ulong addr){ return 0; }
extc t_fixup* cdecl _Findfixup(t_module *pmod,ulong addr){ return 0; }
extc uchar*  cdecl _Finddecode(ulong addr,ulong *psize){ return 0; }
extc ulong   cdecl _Findfileoffset(t_module *pmod,ulong addr){ return 0; }
extc int     cdecl _Decoderange(ulong addr,ulong size,char *s){ return 0; }
extc int     cdecl _Analysecode(t_module *pm){ return 0; }
extc int     cdecl _Registerotclass(char *classname,	char *iconname,WNDPROC classproc){ return 0; }
extc HWND    cdecl _Newtablewindow(t_table *pt,int nlines,int maxcolumns,	char *winclass,char *wintitle){ return 0; }
extc HWND    cdecl _Quicktablewindow(t_table *pt,int nlines,int maxcolumns,	char *winclass,char *wintitle){ return 0; }
extc HWND    cdecl _Createdumpwindow(char *name,ulong base,ulong size,	ulong addr,int type,SPECFUNC *specdump){ return 0; }
extc void    cdecl _Setdumptype(t_dump *pd,int dumptype){}
extc void    cdecl _Dumpbackup(t_dump *pd,int action){}
extc int     cdecl _Broadcast(UINT msg,WPARAM wp,LPARAM lp){ return 0; }
extc ulong   cdecl _Compress(uchar *bufin,ulong nbufin,	uchar *bufout,ulong nbufout){ return 0; }
extc ulong   cdecl _Getoriginaldatasize(char *bufin,ulong nbufin){ return 0; }
extc ulong   cdecl _Decompress(uchar *bufin,ulong nbufin,	uchar *bufout,ulong nbufout){ return 0; }
extc ulong   cdecl _Calculatecrc(uchar *copy,ulong base,ulong size,	t_module *pmod,ulong fixupoffset){ return 0; }
extc int     cdecl _Findreferences(ulong base,ulong size,ulong addr0,ulong addr1,	ulong origin,int recurseonjump,char *title){ return 0; }
extc int     cdecl _Findstrings(ulong base,ulong size,ulong origin,char *title){ return 0; }
extc int     cdecl _Findalldllcalls(t_dump *pd,ulong origin,char *title){ return 0; }
extc int     cdecl _Findallcommands(t_dump *pd,t_asmmodel *model,	ulong origin,char *title){ return 0; }
extc int     cdecl _Findallsequences(t_dump *pd,t_extmodel model[NSEQ][NMODELS],	ulong origin,char *title){ return 0; }
extc ulong   cdecl _Walkreference(int dir){ return 0; }
extc ulong   cdecl _Walkreferenceex(int dir,ulong *size){ return 0; }


extc int     cdecl _Setbreakpoint(ulong addr,ulong type,uchar cmd){ return 0; }
extc int     cdecl _Setbreakpointext(ulong addr,ulong type,char cmd,	ulong passcount){ return 0; }
extc int     cdecl _Manualbreakpoint(ulong addr,	int key,int shiftkey,ulong nametype,int font){ return 0; }
extc void    cdecl _Deletebreakpoints(ulong addr0,ulong addr1,int silent){}
extc ulong   cdecl _Getbreakpointtype(ulong addr){ return 0; }
extc ulong   cdecl _Getbreakpointtypecount(ulong addr,ulong *passcount){ return 0; }
extc ulong   cdecl _Getnextbreakpoint(ulong addr,ulong *type,int *cmd){ return 0; }
extc void    cdecl _Tempbreakpoint(ulong addr,int mode){}
extc int     cdecl _Hardbreakpoints(int closeondelete){ return 0; }
extc int     cdecl _Sethardwarebreakpoint(ulong addr,int size,int type){ return 0; }
extc int     cdecl _Deletehardwarebreakpoint(int index){ return 0; }
extc int     cdecl _Deletehardwarebreakbyaddr(ulong addr){ return 0; }
extc int     cdecl _Setmembreakpoint(int type,ulong addr,ulong size){ return 0; }
extc uchar*  cdecl _Findhittrace(ulong addr,uchar **ptracecopy,ulong *psize){ return 0; }
extc int     cdecl _Modifyhittrace(ulong addr0,ulong addr1,int mode){ return 0; }
extc ulong   cdecl _Isretaddr(ulong retaddr,ulong *procaddr){ return 0; }
extc HWND    cdecl _Creatertracewindow(void){ return 0; }
extc void    cdecl _Settracecondition(char *cond,int onsuspicious,	ulong in0,ulong in1,ulong out0,ulong out1){}
extc void    cdecl _Settracecount(ulong count){}
extc void    cdecl _Settracepauseoncommands(char *cmdset){}
extc int     cdecl _Startruntrace(t_reg *preg){ return 0; }
extc void    cdecl _Deleteruntrace(void){}
extc int     cdecl _Runtracesize(void){ return 0; }
extc int     cdecl _Findprevruntraceip(ulong ip,int startback){ return 0; }
extc int     cdecl _Findnextruntraceip(ulong ip,int startback){ return 0; }
extc int     cdecl _Getruntraceregisters(int nback,t_reg *preg,	t_reg *pold,char *cmd,char *comment){ return 0; }
extc int     cdecl _Getruntraceprofile(ulong addr,ulong size,ulong *profile){ return 0; }
extc void    cdecl _Scrollruntracewindow(int back){}
extc HWND    cdecl _Createprofilewindow(ulong base,ulong size){ return 0; }
extc int     cdecl _Injectcode(ulong threadid,t_inject *inject,char *data,	ulong datasize,ulong parm1,ulong parm2,	INJECTANSWER *answerfunc){ return 0; }



extc void    cdecl _Setcpu(ulong threadid,ulong asmaddr,	ulong dumpaddr,ulong stackaddr,int mode){}
extc void    cdecl _Setdisasm(ulong asmaddr,ulong selsize,int mode){}
extc void    cdecl _Redrawdisassembler(void){}
extc void    cdecl _Getdisassemblerrange(ulong *pbase,ulong *psize){}
extc ulong   cdecl _Findprocbegin(ulong addr){ return 0; }
extc ulong   cdecl _Findprocend(ulong addr){ return 0; }
extc ulong   cdecl _Findprevproc(ulong addr){ return 0; }
extc ulong   cdecl _Findnextproc(ulong addr){ return 0; }
extc int     cdecl _Getproclimits(ulong addr,ulong *start,ulong *end){ return 0; }
extc void    cdecl _Sendshortcut(int where,ulong addr,	int msg,int ctrl,int shift,int vkcode){}
extc t_status cdecl _Getstatus(void){ return STAT_RUNNING; }
extc int     cdecl _Go(ulong threadid,ulong tilladdr,int stepmode,	int givechance,int backupregs){ return 0; }
extc int     cdecl _Suspendprocess(int processevents){ return 0; }


extc uchar*  cdecl _Findknownfunction(ulong addr,int direct,	int level,char *fname){ return 0; }
extc int     cdecl _Decodeknownargument(ulong addr,uchar *arg,ulong value,	int valid,char *s,char *mask,uchar *pset[]){ return 0; }
extc char    cdecl *_Findunknownfunction(ulong ip,char *code,char *dec,	ulong size,char *fname){ return 0; }
extc int     cdecl _Decodeascii(ulong value,char *s,int len,int mode){ return 0; }
extc int     cdecl _Decodeunicode(ulong value,char *s,int len){ return 0; }
extc HWND    cdecl _Showsourcefromaddress(ulong addr,int show){ return 0; }
extc int     cdecl _Getresourcestring(t_module *pm,ulong id,char *s){ return 0; }
extc t_sourceline* cdecl _Getlinefromaddress(ulong addr){ return 0; }
extc ulong   cdecl _Getaddressfromline(ulong addr0,ulong addr1,	char *path,ulong line){ return 0; }
extc int     cdecl _Getsourcefilelimits(ulong nameaddr,	ulong *addr0,ulong *addr1){ return 0; }
extc int     cdecl _Decodefullvarname(t_module *pmod,t_symvar *psym,	int offset,char *name){ return 0; }
extc int     cdecl _Getbprelname(t_module *pmod,ulong addr,long offset,	char *s,int nsymb){ return 0; }
extc HWND    cdecl _Createwatchwindow(void){ return 0; }
extc int     cdecl _Deletewatch(int indexone){ return 0; }
extc int     cdecl _Insertwatch(int indexone,char *text){ return 0; }
extc int     cdecl _Getwatch(int indexone,char *text){ return 0; }
extc HWND    cdecl _Createwinwindow(void){ return 0; }
extc HWND    cdecl _Createpatchwindow(void){ return 0; }

char aOt_plugin_04i[] = "OT_PLUGIN_%04i";
CHAR aIco_plugin[] = "ICO_PLUGIN";
DWORD dword_4F55B8 = 0;

extc int     cdecl _Registerpluginclass(char *classname, char *lpIconName,	HINSTANCE hInstance, WNDPROC classproc)
{
	int result;
	WNDCLASSA WndClass;

	if ( classname && classproc )
	{
		sprintf(classname, aOt_plugin_04i, dword_4F55B8);
		WndClass.style = 11;
		++dword_4F55B8;
		WndClass.lpfnWndProc = (WNDPROC)classproc;
		WndClass.cbClsExtra = 0;
		WndClass.cbWndExtra = 32;
		WndClass.hInstance = ::hInstance;
		if ( lpIconName && *lpIconName )
			WndClass.hIcon = LoadIconA(hInstance, lpIconName);
		else
			WndClass.hIcon = LoadIconA(::hInstance, aIco_plugin);
		WndClass.hCursor = 0;
		WndClass.hbrBackground = 0;
		WndClass.lpszMenuName = 0;
		WndClass.lpszClassName = classname;
		if ( RegisterClassA(&WndClass) )
		{
			result = 0;
		}
		else
		{
			*classname = 0;
			result = -1;
		}
	}
	else
	{
		result = -1;
	}
	return result;

}

extc void    cdecl _Unregisterpluginclass(LPCSTR lpClassName)
{
	if ( lpClassName )
	{
		UnregisterClassA(lpClassName, hInstance);
	}
}

char aI_6[] = "%i";
extc int     cdecl _Pluginwriteinttoini(HINSTANCE dllinst,char *key,int value)
{ 
	
	/*
	sprintf(&AppName, aPluginS, (char *)&unk_4F0BBC + 600 * v4);
	sprintf(&String, aI_6, a3);
	WritePrivateProfileStringA(&AppName, lpKeyName, &String, FileName);
	return 1;
	*/
	
	return 0;
}
extc int     cdecl _Pluginwritestringtoini(HINSTANCE dllinst,char *key,char *s)
{ 
	
	/*
	sprintf(&AppName, aPluginS, (char *)&unk_4F0BBC + 600 * v4);
	if ( a3 )
	v6 = (const CHAR *)a3;
	else
	v6 = &a_dll[5];
	WritePrivateProfileStringA(&AppName, lpKeyName, v6, FileName);
	result = 1;
	*/
	
	return 0;
}


extc int     cdecl _Pluginreadintfromini(HINSTANCE dllinst,char *key,int def)
{ 
	
	//sprintf(&AppName, aPluginS, (char *)&unk_4F0BBC + 600 * v4);
	//return GetPrivateProfileIntA(&AppName, lpKeyName, nDefault, FileName);
	return 0;
}



extc int     cdecl _Pluginreadstringfromini(HINSTANCE dllinst,char *key,	char *s,char *def)
{ 
	/*
	sprintf(&AppName, aPluginS, (char *)&unk_4F0BBC + 600 * v5);
	return GetPrivateProfileStringA(&AppName, lpKeyName, lpDefault, lpReturnedString, 0x100u, FileName);
	*/
	
	
	return 0; 
}
extc int     cdecl _Pluginsaverecord(ulong tag,ulong size,void *data){ return 0; }


extc DWORD cdecl _Plugingetvalue(int type)
{
	DWORD result = 0;

	switch ( type )
	{
	case 1://VAL_HINST
		result = (DWORD)hInstance;
		break;
	case 2:
		result = (DWORD)hWndMain;
		break;
	case 3:
		result = (DWORD)hWndClient;
		break;
	case 4:
		result = 20;
		break;
	case 5:
		//result = &dword_4D3B94;
		break;
	case 6:
		//result = &hbr;
		break;
	case 7:
		//result = &dword_4D3C34;
		break;
	case 8:
		result = 11;
		break;
	case 9:
		//result = &dword_4D409C;
		break;
	case 10:
		//result = &unk_4D40C8;
		break;
	case 11:
		//result = &X;
		break;
	case 12:
		//result = dword_4D4920;
		break;
	case 13:
		result = 8;
		break;
	case 14:
		//result = (LPCVOID)dword_4D5398;
		break;
	case 15:
		result = 8;
		break;
	case 16:
		//result = &unk_4D4A74;
		break;
	case 17:
		//result = (LPCVOID)nDefault;
		break;
	case 18:
		//result = (LPCVOID)dword_4D54B4;
		break;
	case 19:
		//result = (LPCVOID)dword_4D54B8;
		break;
	case 20:
		result = (DWORD)hProcess;
		break;
	case 21:
		//result = (LPCVOID)dword_4D5A70;
		break;
	case 22:
		//result = (LPCVOID)dword_4D5A6C;
		break;
	case 23:
		//result = (LPCVOID)dword_4D5A74;
		break;
	case 24:
		//result = dword_4D7364;
		break;
	case 25:
		//result = &byte_4D5A7C;
		break;
	case 26:
		//result = &MultiByteStr;
		break;
	case 27:
		//result = &byte_4D5C84;
		break;
	case 28:
		//result = &Buffer;
		break;
	case 29:
		//result = (LPCVOID)dword_4D919C;
		break;
	case 30:
		//result = (LPCVOID)dword_4D91CC;
		break;
	case 31:
		//result = (LPCVOID)dword_4DE5B0;
		break;
	case 32:
		//result = (LPCVOID)dword_4DE5B4;
		break;
	case 33:
		//result = (LPCVOID)dword_4E276C;
		break;
	case 34:
		//result = (LPCVOID)dword_4E2770;
		break;
	case 35:
		//result = (LPCVOID)dword_4D91C0;
		break;
	case 36:
		//result = (LPCVOID)dword_4E2774;
		break;
	case 37:
		//result = (LPCVOID)dword_4E2778;
		break;
	case 38:
		//result = (LPCVOID)dword_4E277C;
		break;
	case 39:
		//result = (LPCVOID)dword_4DE5BC;
		break;
	case 40:
		//result = (LPCVOID)dword_4E2DF4;
		break;
	case 41:
		//result = &dword_4D70F8;
		break;
	case 42:
		//result = &dword_4D77E0;
		break;
	case 43:
		//result = &dword_4D7C90;
		break;
	case 44:
		//result = &dword_4D7EDD;
		break;
	case 45:
		//result = &dword_4DE5C8;
		break;
	case 46:
		//result = &unk_4E2AA0;
		break;
	case 47:
		//result = &dword_4D8DE0;
		break;
	case 50:
		//result = (LPCVOID)unk_4D54D4;
		break;
	case 51:
		//result = dword_4D9E40;
		break;
	case 52:
		//result = (LPCVOID)dword_4DE5B8;
		break;
	case 53:
		//result = sub_41D0C8(a1);
		break;
	case 54:
		//result = sub_41D0C8(a1);
		break;
	case 55:
		//result = sub_41D0C8(a1);
		break;
	case 56:
		//result = &szHelp;
		break;
	case 57:
		//result = (LPCVOID)dword_4D375C;
		break;
	case 58:
		//result = &dword_4E3168;
		break;
	case 59:
		//result = byte_4D9034;
		break;
	default:
		result = 0;
		break;
	}

	return result;
}


extc int     cdecl _Decodeaddress(ulong addr,ulong base,int addrmode,	char *symb,int nsymb,char *comment){ return 0; }
extc int     cdecl _Decoderelativeoffset(ulong addr,int addrmode,	char *symb,int nsymb){ return 0; }
extc int     cdecl _Decodecharacter(char *s,uint c){ return 0; }
extc int     cdecl _Printfloat4(char *s,float f){ return 0; }
extc int     cdecl _Printfloat8(char *s,double d){ return 0; }
extc int     cdecl _Printfloat10(char *s,long double ext){ return 0; }
extc int     cdecl _Print3dnow(char *s,uchar *f){ return 0; }
extc int     cdecl _Printsse(char *s,char *f){ return 0; }
extc ulong   cdecl _Followcall(ulong addr){ return 0; }
extc int     cdecl _IstextA(char c){ return 0; }
extc int     cdecl _IstextW(wchar_t w){ return 0; }
extc int     cdecl _Stringtotext(char *data,int ndata,char *text,int ntext){ return 0; }

extc ulong   cdecl _Disasm(uchar *src,ulong srcsize,ulong srcip,uchar *srcdec,	t_disasm *disasm,int disasmmode,ulong threadid){ return 0; }
extc ulong   cdecl _Disassembleback(uchar *block,ulong base,ulong size,	ulong ip,int n,int usedec){ return 0; }
extc ulong   cdecl _Disassembleforward(uchar *block,ulong base,ulong size,	ulong ip,int n,int usedec){ return 0; }
extc int     cdecl _Issuspicious(char *cmd,ulong size,ulong ip,	ulong threadid,t_reg *preg,char *s){ return 0; }
extc int     cdecl _Isfilling(ulong offset,char *data,ulong size,ulong align){ return 0; }

int __cdecl _Isprefix(unsigned __int8 a1)
{
	if ( (signed int)a1 > 62 )
	{
		if ( (unsigned int)a1 - 100 >= 4 && a1 != 240 && (unsigned int)a1 - 242 >= 2 )
			return 0;
		return 1;
	}
	if ( a1 == 62 || a1 == 38 || a1 == 46 || a1 == 54 )
		return 1;
	return 0;
}